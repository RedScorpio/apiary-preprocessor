'use strict';

var ApiaryExtension = require('./extensions/apiary');

function CallInformation(responseContent) {
  this.responseContent = responseContent;
  var request = responseContent.parents(ApiaryExtension.REQUEST)[0];
  var sections = responseContent.parents(ApiaryExtension.SECTION);
  for (var i = 0; i < sections.length && (!this.method || !this.path); i++) {
    var req = sections[i].request;
    if (!this.path && req.address) {
      this.path = req.address;
    }
    if (!this.method && req.method) {
      this.method = req.method;
    }
  }
  if (request) {
    this.requestHeadersContent = request.getHeadersContent();
    this.requestBodyContent = request.getBodyContent();
  }
  this._reqHeaders = this.requestHeadersContent ? this.requestHeadersContent.getHeaders() : {};
  this._reqBody = this.requestBodyContent ? this.requestBodyContent.getBody() : {};
  this._reqBodyIsJson = typeof this._reqBody !== 'string';

  this._resCode = this.responseContent.code;
  this.bodyContent = this.responseContent.getBodyContent();
  this.headersContent = this.responseContent.getHeadersContent();
}
CallInformation.prototype.setResponseBody = function (body) {
  this.bodyContent.setBody(body);
};
CallInformation.prototype.setResponseHeaders = function (headers) {
  this.headersContent.setHeaders(headers);
};

Object.defineProperty(CallInformation.prototype, 'headers', {
  get: function () {
    return this._reqHeaders;
  },
  set: function (value) {
    this._reqHeaders = ApiaryExtension.processHeaders(value);
    if (this.requestHeadersContent)
      this.requestHeadersContent.setHeaders(this._reqHeaders);
    return this._reqHeaders;
  }
});

Object.defineProperty(CallInformation.prototype, 'code', {
  get: function () {
    return this._resCode;
  }
});

Object.defineProperty(CallInformation.prototype, 'body', {
  get: function () {
    return this._reqBody;
  },
  set: function (value) {
    if (this._reqBodyIsJson && typeof value === 'string') {
      value = JSON.parse(value);
    }
    this._reqBody = value;
    if (this.requestBodyContent)
      this.requestBodyContent.setBody(this._reqBody);
    return this._reqBody;
  }
});


module.exports = CallInformation;