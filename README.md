# Apiary Preprocessor

Made to make updating Apiary documentation simpler. What does it do?

* Loads Apiary blueprint file
* Fixes all inconsistencies - removes multiple spaces, empty lines
* Finds all `+ Response` tags and matches them to requests, and then makes calls to server specified in options
* Offers a set of callbacks that are run before/after each call, on error and before/after all calls 
* For calls that need authorization you have to override callInformation headers and/or body

## Sample Usage

TODO :)